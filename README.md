#basicapp's README

### Clone the repo ###

* Navigate to the repository in Bitbucket.
* Click the Clone button.
* Copy the clone command (either the SSH format or the HTTPS).
* Launch a terminal window.
* Change to the local directory where you want to clone your repository.
* Paste the command you copied from Bitbucket