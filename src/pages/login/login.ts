import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { RegisterPage } from '../register/register';
import { Page1 } from '../page1/page1';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  gotoReg(){
    this.navCtrl.push(RegisterPage);
  }

  gotoPage1(){
    this.navCtrl.push(Page1);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

}
